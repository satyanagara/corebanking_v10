from odoo import fields, models, api, _
from decimal import *


class LoanProduct(models.Model):
    _name = 'corebanking.loan_product'
    _description = 'Loan Product'
    
    
    @api.multi
    def copy(self, default=None):
        self.ensure_one()
        default = dict(default or {}, name=_('%s (copy)') % self.name)
        return super(LoanProduct, self).copy(default)
    
    name = fields.Char(string='Name', required=1)
    loan_interest = fields.Float(string='Interest (%)', default=15.00)
    account_record_ids = fields.One2many('corebanking.loan_product.account_rec','product_id',string='Account Records')
    

class AccountRecord(models.Model):
    _name = 'corebanking.loan_product.account_rec'
    
    STAGE = [('contract','Contract'),('installment','Installment'),('withdrawal','Withdrawal'),('amort','Amortisation'),('payment','Payment')]
    
    product_id = fields.Many2one('corebanking.loan_product',string='Product',required=1,ondelete='cascade')
    name = fields.Char(string='Description',required=1)
    journal_id = fields.Many2one('account.journal',string='Journal',required=1)
    line_ids = fields.One2many('corebanking.account_rec.line','rec_id',string='Records',required=1)
    stage = fields.Many2one('corebanking.loan_stage',string='Stage',required=1)
    
class AccountRecordLine(models.Model):
    _name = 'corebanking.account_rec.line'
    
    rec_id = fields.Many2one('corebanking.loan_product.account_rec',string='Record',required=1,ondelete='cascade')
    name = fields.Char(string='Label',required=1)
    account_id = fields.Many2one('account.account',string='Account',required=1)
    position = fields.Selection([('debit','Debit'),('credit','Credit')],string='Position',default='debit',required=1)
    
