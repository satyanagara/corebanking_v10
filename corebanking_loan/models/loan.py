from odoo import fields, models, api, _
from decimal import *
from odoo.exceptions import ValidationError

class LoanStage(models.Model):
    _name = 'corebanking.loan_stage'
    _order = 'sequence ASC'
    
    name = fields.Char(string='Stage',required=1)
    sequence = fields.Integer(string='Order',default=5)

class Loan(models.Model):
    _name = 'corebanking.loan'
    _description = 'Loan Management'
    
    @api.model
    def amortize_flat(self):
        amount_loan = self.amount_loan
        monthly_interest = (amount_loan * (self.loan_interest / 100.0)) / self.term # 0.12
        print "====INTEREST==",monthly_interest
        term = self.term
        
        monthly_payment = amount_loan / term
        
        start_balance = amount_loan
        
        vals = []
        
        for x in range(0, term):
            end_balance = start_balance - monthly_payment
            amount = monthly_interest + monthly_payment
            vals.append((0, 0, {
                'payment':round(monthly_payment, 0),
                'interest':round(monthly_interest , 0),
                'amount':round(amount, 0),
                'beg_bal':round(start_balance, 0),
                'end_bal':round(end_balance, 0),
                
                              }))
            start_balance = end_balance
        return vals
    
    
    @api.model
    def _create_entry(self,stage=None):
        entry_obj = self.env['account.move']
        moves = []
        #journal loan
        for j in self.product_id.account_record_ids.filtered(lambda r:r.stage == 'contract'):
            lines = []
            for line in j.line_ids:
                if line.position == 'debit':
                    debit = self.amount_expense
                    credit = 0.0
                else:
                    credit = self.amount_expense
                    debit = 0.0
                lines.append((0,0,{'partner_id':self.partner_id.id,'account_id':line.account_id.id,'name':line.name,'debit':debit,'credit':credit}))
            
            move_vals = {
                'journal_id':j.journal_id.id,
                'narration':j.name,
                'ref':self.name,
                'line_ids':lines
                }
            move = entry_obj.create(move_vals)
            moves.append(move.id)

        if len(moves) > 0:
            self.move_ids = moves
        else:
            raise ValidationError('Please check the entries in product!')
    
    @api.one
    @api.depends('pay_sched_ids')
    def _compute_all(self):
        if self.pay_sched_ids:
            amount_interest = 0.0
            for p in self.pay_sched_ids:
                amount_interest += p.interest
            self.amount_interest = amount_interest
    
    @api.multi
    @api.onchange('product_id')
    def onchange_product(self):
        if self.partner_id:
            self.journal_plan_ids.unlink()
            if not self.partner_id:
                    raise ValidationError('Please, select Debtor first!')
            print "====== HEREEEEEEEEEEE ========="
                #self.loan_interest = self.product_id.loan_interest
                
                #INSERT journal template
            recs = []
            for rec in self.product_id.account_record_ids:
                lines = []
                for l in rec.line_ids:
                    lines.append((0,0,{
                         'account_id':l.account_id.id,
                         'partner_id':self.partner_id.id,
                         'name':l.name,
                         'debit':0.0,
                         'credit':0.0
                         }))
                 
                recs.append((0,0,
                              {
                             'stage':rec.stage.id,
                              'journal_id':rec.journal_id.id,    
                               'entry_ids':lines,
                               'ref':self.name,
                               'narration':rec.name,
                                  }
                              ))
            print "===== RECS =====",recs
            self.journal_plan_ids = recs

    @api.multi
    def action_compute_payment(self):
        self.pay_sched_ids.unlink()
        sched = []
        if self.loan_method == 'flat':
            sched = self.amortize_flat()
        
        self.pay_sched_ids = sched
        
   
#     @api.multi
#     def action_confirm(self):
#         name = self.env['ir.sequence'].next_by_code('corebanking.loan')
#         self.name = name
#         
#         
#     @api.multi
#     def action_installment(self):
#         self.state = 'installment'
#         
#     @api.multi
#     def action_withdraw(self):
#         self.state = 'withdraawal'
#     
#     @api.multi
#     def action_npl(self):
#         self.state = 'npl'
#         
#     @api.multi
#     def action_done(self):
#         self.state = 'close'

    def _default_stage(self):
        stage = self.env['corebanking.loan_stage'].search([],0,1)
        if stage:
            return stage.id
        #return False
    
    name = fields.Char(string='Number', required=1, readonly=1, default='/')
    product_id = fields.Many2one('corebanking.loan_product', string='Loan Product', required=1)
    date_loan = fields.Date(string='Date')
    partner_id = fields.Many2one('res.partner', string='Debtor', required=1)
    amount_loan = fields.Float(string='Loan Amount')
    amount_interest = fields.Float(string='Income from Interest', compute='_compute_all', store=1)
    amount_total = fields.Float(string='Amount Total', compute='_compute_expense', store=1)
    #amount_expense = fields.Float(string='Amount Expense')
    tax_ids = fields.Many2many('account.tax', 'corebanking_loan_tax_rel', 'loan_id', 'tax_id', string='Taxes')
    loan_interest = fields.Float(string='Interest (%)')
    loan_method = fields.Selection([('flat', 'Flat')], string='Method', default='flat',required=1)
    pay_sched_ids = fields.One2many('corebanking.pay_sched', 'loan_id', string='Schedule')
    currency_id = fields.Many2one('res.currency', string='Currency')
    loan_interest = fields.Float(string='Interest (%)')
    #state = fields.Selection(STATE, default='draft', readonly=1)
    stage = fields.Many2one('corebanking.loan_stage',string='Stage',required=1,default=_default_stage)
    user_id = fields.Many2one('res.users', string='Loan Staff')
    term = fields.Integer(string='Term in Months', default=12)
    type = fields.Selection([('normal','Normal'),('syn','Syndication')],string='Type',required=1,default='normal')
    move_ids = fields.Many2many('account.move','corebanking_loan_move_rel','loan_id','move_id',string='Entries')
    journal_plan_ids = fields.One2many('corebanking.loan.journal','loan_id',string='Journal',required=1)
    

class LoanJournal(models.Model):
    _name = 'corebanking.loan.journal'
    
    name = fields.Char(string='Description',required=1)
    loan_id = fields.Many2one('corebanking.loan',string='Loan',required=1,ondelete='cascade')
    journal_id = fields.Many2one('account.journal',string='Journal',required=1)
    stage = fields.Many2one('corebanking.loan_stage',string='Stage',required=1)
    entry_ids = fields.One2many('corebanking.loan.journal_entry','loan_journal_id',string='Entries',required=1)

class LoanJournalEntries(models.Model):
    _name = 'corebanking.loan.journal_entry'
    
    loan_journal_id = fields.Many2one('corebanking.loan.journal',string='Loan Journal',required=1,ondelete='cascade')
    name = fields.Char(string='Label',required=1)
    account_id = fields.Many2one('account.account',string='Account',required=1)
    debit = fields.Float(string='Debit')
    credit = fields.Float(string='Credit')
    ref = fields.Char(string='Narration',required=1)

class Sched(models.Model):
    _name = 'corebanking.pay_sched'
    _description = 'Loan Payment Schedule'
    
    loan_id = fields.Many2one('corebanking.loan', string='Loan', required=1, ondelete='cascade')
    beg_bal = fields.Float(string='Beg. Balance')
    payment = fields.Float(string='Payment')
    amount = fields.Float(string='Amount')
    interest = fields.Float(string='Interest')
    end_bal = fields.Float(string='End Balance')
