{
    'name': "Corebanking - Loan Management",
    'summary': """Custom""",
    'description': """Description goes here""",
    'author': "ERPCentral",
    'website': "http://www.erpcentral.biz",
    'category': 'Custom',
    'version': '0.1',
    'depends': ['base','corebanking','account','account_accountant'],
    'data': [
        'data/seq.xml',
        'views/menu.xml',
        'views/loan_stage_view.xml',
        'views/loan_product_view.xml',
        'views/loan_view.xml',
        
        ],
    'demo': [],
}